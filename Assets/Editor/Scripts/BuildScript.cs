using UnityEditor;
class BuildScript
{
     static void PerformBuild ()
     {
         string[] scenes = { "Assets/Scenes/SampleScene.unity" };
         BuildPipeline.BuildPlayer(scenes, "Builds/LinuxBuild", BuildTarget.StandaloneLinux64, BuildOptions.None);
     }
}
