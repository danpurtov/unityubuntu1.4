using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    void Update()
    {
        //transform.Translate(Vector3.forward * Time.deltaTime*Input.GetAxis("Horizontal")*(-1));
        transform.Translate(Vector3.right * Time.deltaTime*Input.GetAxis("Horizontal"));
        transform.Translate(Vector3.forward * Time.deltaTime*Input.GetAxis("Vertical"));
    }
}
