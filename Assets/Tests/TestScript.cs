using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TestScript
{
    public Robot robot;
    public Car car;
    // A Test behaves as an ordinary method
    [Test]
    public void TestScriptSimplePasses()
    {
        // Use the Assert class to test conditions
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    [UnityTest]
    public IEnumerator TestScriptWithEnumeratorPasses()
    {
        //var ter = GameObject.GetComponents<var>();
        //GameObject car = GameObject.Find("Car");
        //GameObject robot = GameObject.Find("Robot Kyle");
        GameObject carGameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Car"));
        car = carGameObject.GetComponent<Car>();
        GameObject robotGameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Robot Kyle"));
        robot = robotGameObject.GetComponent<Robot>();
        Assert.IsNotNull(robot);
        //float initYPos = car.transform.position.y;
        //float initXPos = car.transform.position.x;
        //yield return new WaitForSeconds(0.1f);
        //Assert.Less(car.transform.position.y,ter.transform.position.y);
        //Assert.Less(car.transform.position.x,ter.transform.position.x);
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;
    }
}
